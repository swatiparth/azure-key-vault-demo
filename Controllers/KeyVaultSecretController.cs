﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Azure_Key_Vault_Demo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class KeyVaultSecretController : Controller
    {
        private readonly IConfiguration _configuration;

        public KeyVaultSecretController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public string Get()
        {
            var value = _configuration["SecretKeyName"];
            return "Value for Secret [SecretKeyName] is : " + value;
        }
    }
}
